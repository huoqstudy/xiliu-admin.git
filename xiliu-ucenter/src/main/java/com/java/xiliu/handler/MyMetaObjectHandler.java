package com.java.xiliu.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.java.xiliu.common.untils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author huoqiang
 * @description
 * @date 2021/9/1
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        this.setFieldValByName("createdTime", new Date(), metaObject);
        this.setFieldValByName("deleted", false, metaObject);
        Object createUser = this.getFieldValByName("createdBy", metaObject);
        if (createUser == null) {
            if(SecurityUtils.isLogin()) {
                this.setFieldValByName("createdBy",SecurityUtils.getUserId(),metaObject);
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        this.setFieldValByName("updatedTime", new Date(), metaObject);
        Object createUser = this.getFieldValByName("updatedBy", metaObject);
        if (createUser == null) {
            if(SecurityUtils.isLogin()) {
                this.setFieldValByName("updatedBy",SecurityUtils.getUserId(),metaObject);
            }
        }
    }
}
