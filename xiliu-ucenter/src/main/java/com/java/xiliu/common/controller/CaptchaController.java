package com.java.xiliu.common.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.util.IdUtil;
import com.java.xiliu.common.R;
import com.java.xiliu.common.entity.Captcha;
import com.java.xiliu.common.service.CaptchaService;
import com.java.xiliu.common.untils.CaptchaUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xiliu
 * @description 验证码前端控制器
 * @date 2022/7/7
 */
@RestController
@RequestMapping("/captcha")
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @ApiOperation(value = "生成验证码拼图")
    @PostMapping("get-captcha")
    public R getCaptcha(@RequestBody Captcha captcha) {
        return R.ok(captchaService.getCaptcha(captcha));
    }

}
