package com.java.xiliu.modules.basicdata.service.impl;

import java.util.List;
import java.util.Arrays;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.xiliu.modules.basicdata.entity.XlCity;
import com.java.xiliu.modules.basicdata.mapper.XlCityMapper;
import com.java.xiliu.modules.basicdata.service.IXlCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 城市选择表Service业务层处理
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Service
public class XlCityServiceImpl extends ServiceImpl<XlCityMapper, XlCity> implements IXlCityService {
    @Autowired
    private XlCityMapper xlCityMapper;

    /**
     * 查询城市选择表
     * @param cityId 城市选择表主键
     * @return 城市选择表
     */
    @Override
    public XlCity selectXlCityByCityId(Long cityId) {
        return xlCityMapper.selectXlCityByCityId(cityId);
    }

    /**
     * 查询城市选择表列表
     * @param xlCity 城市选择表
     * @return 城市选择表
     */
    @Override
    public List<XlCity> selectXlCityList(XlCity xlCity) {
        return xlCityMapper.selectXlCityList(xlCity);
    }

    /**
     * 新增城市选择表
     * @param xlCity 城市选择表
     * @return 结果
     */
    @Override
    public boolean create(XlCity xlCity) {
        return save(xlCity);
    }

    /**
     * 修改城市选择表
     * @param xlCity 城市选择表
     * @return 结果
     */
    @Override
    public boolean update(XlCity xlCity) {
        return updateById(xlCity);
    }

    /**
     * 批量删除城市选择表
     * @param cityIds 需要删除的城市选择表主键
     * @return 结果
     */
    @Override
    public int deleteXlCityByCityIds(Long[] cityIds) {
        return xlCityMapper.deleteBatchIds(Arrays.asList(cityIds));
    }

    /**
     * 删除城市选择表信息
     * @param cityId 城市选择表主键
     * @return 结果
     */
    @Override
    public int deleteXlCityByCityId(Long cityId) {
        return xlCityMapper.deleteById(cityId);
    }
}
