package com.java.xiliu.modules.ucenter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.ucenter.entity.XlRole;

import java.util.List;
import java.util.Set;


/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-01
 */
public interface XlRoleService extends IService<XlRole> {

    /**
     * 添加角色
     * @param role
     * @return boolean
     **/
    boolean create(XlRole role);

    /**
     * 批量删除角色
     * @param ids
     * @return boolean
     **/
    boolean delete(List<Long> ids);

    /**
     * 分页获取角色列表
     * @param keyword
     * @param pageSize
     * @param pageNum
     * @return Page
     **/
    Page<XlRole> list(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 分配权限信息
     * @param role 角色信息
     * @return 结果
     */
    boolean authRoleMenu(XlRole role);

    /**
     * 根据用户ID查询角色权限
     * @param userId 用户ID
     * @return 权限列表
     */
    Set<String> selectRolePermissionByUserId(Long userId);

    /**
     * 根据条件分页查询角色数据
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    List<XlRole> selectRoleList(XlRole role);
}
