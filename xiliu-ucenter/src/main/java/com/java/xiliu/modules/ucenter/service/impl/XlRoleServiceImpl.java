package com.java.xiliu.modules.ucenter.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.xiliu.modules.ucenter.entity.XlRoleMenu;
import com.java.xiliu.modules.ucenter.mapper.XlRoleMapper;
import com.java.xiliu.modules.ucenter.entity.XlRole;
import com.java.xiliu.modules.ucenter.mapper.XlRoleMenuMapper;
import com.java.xiliu.modules.ucenter.service.XlRoleMenuService;
import com.java.xiliu.modules.ucenter.service.XlRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-01
 */
@Service
public class XlRoleServiceImpl extends ServiceImpl<XlRoleMapper, XlRole> implements XlRoleService {

    @Autowired
    private XlRoleMenuService roleMenuService;

    @Override
    public boolean create(XlRole role) {
        return save(role);
    }

    @Override
    public boolean delete(List<Long> ids) {
        return removeByIds(ids);
    }

    @Override
    public Page<XlRole> list(String keyword, Integer pageSize, Integer pageNum) {
        Page<XlRole> page = new Page<>(pageNum,pageSize);
        LambdaQueryWrapper<XlRole> wrapper = new LambdaQueryWrapper<XlRole>();
        if (StrUtil.isNotBlank(keyword)) {
            wrapper.like(XlRole::getRoleName, keyword);
        }
        return page(page,wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean authRoleMenu(XlRole role) {
        // 先删除
        roleMenuService.remove(new LambdaQueryWrapper<XlRoleMenu>().eq(XlRoleMenu::getRoleId,role.getRoleId()));
        // 在新增
        return insertRoleMenu(role);
    }

    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        List<XlRole> list = baseMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        if (CollectionUtil.isNotEmpty(list)) {
            for (XlRole role : list) {
                permsSet.addAll(Arrays.asList(role.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    @Override
    public List<XlRole> selectRoleList(XlRole role) {
        return baseMapper.selectRoleList(role);
    }

    /**
     * 新增角色菜单信息
     * @param role 角色对象
     */
    public boolean insertRoleMenu(XlRole role) {
        // 新增用户与角色管理
        List<XlRoleMenu> list = new ArrayList<XlRoleMenu>();
        for (Long menuId : role.getMenuIds()) {
            XlRoleMenu rm = new XlRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0) {
            return roleMenuService.saveBatch(list);
        }
        return true;
    }
}
