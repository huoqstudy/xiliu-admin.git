package com.java.xiliu.modules.ucenter.vo;

import com.java.xiliu.common.entity.BasePageEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huoqiang
 * @description
 * @date 2021/9/2
 */
@Data
public class MemberQueryVo extends BasePageEntity {

    @ApiModelProperty(value = "用户名")
    private String userName;
}
