package com.java.xiliu.modules.basicdata.service.impl;

import java.util.List;
import java.util.Arrays;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.java.xiliu.modules.basicdata.mapper.XlZoneMapper;
import com.java.xiliu.modules.basicdata.entity.XlZone;
import com.java.xiliu.modules.basicdata.service.IXlZoneService;

import javax.annotation.Resource;

/**
 * 城区信息表Service业务层处理
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Service
public class XlZoneServiceImpl extends ServiceImpl<XlZoneMapper, XlZone> implements IXlZoneService {

    @Resource
    private XlZoneMapper xlZoneMapper;

    /**
     * 查询城区信息表
     * @param zoneId 城区信息表主键
     * @return 城区信息表
     */
    @Override
    public XlZone selectXlZoneByZoneId(String zoneId) {
        return xlZoneMapper.selectXlZoneByZoneId(zoneId);
    }

    /**
     * 查询城区信息表列表
     * @param xlZone 城区信息表
     * @return 城区信息表
     */
    @Override
    public List<XlZone> selectXlZoneList(XlZone xlZone) {
        return xlZoneMapper.selectXlZoneList(xlZone);
    }

    /**
     * 新增城区信息表
     * @param xlZone 城区信息表
     * @return 结果
     */
    @Override
    public boolean create(XlZone xlZone) {
        return save(xlZone);
    }

    /**
     * 修改城区信息表
     * @param xlZone 城区信息表
     * @return 结果
     */
    @Override
    public boolean update(XlZone xlZone) {
        return updateById(xlZone);
    }

    /**
     * 批量删除城区信息表
     * @param zoneIds 需要删除的城区信息表主键
     * @return 结果
     */
    @Override
    public int deleteXlZoneByZoneIds(String[] zoneIds) {
        return xlZoneMapper.deleteBatchIds(Arrays.asList(zoneIds));
    }

    /**
     * 删除城区信息表信息
     * @param zoneId 城区信息表主键
     * @return 结果
     */
    @Override
    public int deleteXlZoneByZoneId(String zoneId) {
        return xlZoneMapper.deleteById(zoneId);
    }
}
