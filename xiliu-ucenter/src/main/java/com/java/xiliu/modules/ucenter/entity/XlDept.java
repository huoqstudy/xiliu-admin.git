package com.java.xiliu.modules.ucenter.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author xiliu
 * @since 2022-07-12
 */
@Getter
@Setter
@TableName("xl_dept")
@ApiModel(value = "XlDept对象", description = "部门表")
public class XlDept implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    @TableId(value = "dept_id", type = IdType.AUTO)
    private Long deptId;

    @ApiModelProperty("父部门id")
    private Long parentId;

    @ApiModelProperty("祖级列表")
    private String ancestors;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("显示顺序")
    private Integer sort;

    @ApiModelProperty("负责人")
    private String leader;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("部门状态（0正常 1停用）")
    private Integer status;

    @ApiModelProperty("部门级别")
    private Integer deptLevel;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty("修改时间")
    private Date updatedTime;

    @ApiModelProperty("修改人的id")
    private Long updatedBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    @ApiModelProperty("创建人的id")
    private Long createdBy;

    @ApiModelProperty("逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @ApiModelProperty("数据版本")
    private Integer version;

    /** 子部门 */
    @TableField(exist = false)
    private List<XlDept> children = new ArrayList<>();
}
