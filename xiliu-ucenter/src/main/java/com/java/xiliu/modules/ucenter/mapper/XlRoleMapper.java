package com.java.xiliu.modules.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.ucenter.entity.XlRole;

import java.util.List;


/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author xiliu
 * @since 2022-07-01
 */
public interface XlRoleMapper extends BaseMapper<XlRole> {

    /**
     * 通过角色ID查询角色
     * @param roleId 角色ID
     * @return 角色对象信息
     */
     XlRole selectRoleById(Long roleId);

    /**
     * 根据用户ID查询角色
     * @param userId 用户ID
     * @return 角色列表
     */
     List<XlRole> selectRolePermissionByUserId(Long userId);

    /**
     * 根据条件分页查询角色数据
     * @param role 角色信息
     * @return 角色数据集合信息
     */
     List<XlRole> selectRoleList(XlRole role);

}
