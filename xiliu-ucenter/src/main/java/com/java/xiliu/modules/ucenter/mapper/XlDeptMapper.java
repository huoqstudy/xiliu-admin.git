package com.java.xiliu.modules.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.ucenter.entity.XlDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author xiliu
 * @since 2022-07-12
 */
public interface XlDeptMapper extends BaseMapper<XlDept> {

    /**
     * 查询部门管理数据
     * @param dept 部门信息
     * @return 部门信息集合
     */
    List<XlDept> selectDeptList(@Param(value = "dept")XlDept dept);

    /**
     * 根据ID查询所有子部门
     * @param deptId 部门ID
     * @return 部门列表
     */
    List<XlDept> selectChildrenDeptById(Long deptId);

}
