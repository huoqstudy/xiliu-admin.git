package com.java.xiliu.modules.basicdata.controller;

import com.java.xiliu.common.R;
import com.java.xiliu.common.annotation.Log;
import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.page.TableDataInfo;
import com.java.xiliu.modules.basicdata.entity.XlCity;
import com.java.xiliu.modules.basicdata.service.IXlCityService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 城市选择表Controller
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Api(tags = "城市管理")
@RestController
@RequestMapping("/basic/city")
public class XlCityController extends BaseController {
    @Autowired
    private IXlCityService xlCityService;

    /**
     * 查询城市选择表列表
     */
    @PreAuthorize("@customSs.hasPermi('basic:city:list')")
    @GetMapping("/list")
    public TableDataInfo list(XlCity xlCity) {
        startPage();
        List<XlCity> list = xlCityService.selectXlCityList(xlCity);
        return getDataTable(list);
    }


    /**
     * 获取城市选择表详细信息
     */
    @PreAuthorize("@customSs.hasPermi('basic:city:query')")
    @GetMapping(value = "/{cityId}")
    public R getInfo(@PathVariable("cityId") Long cityId) {
        return R.ok(xlCityService.selectXlCityByCityId(cityId));
    }

    /**
     * 新增城市选择表
     */
    @PreAuthorize("@customSs.hasPermi('basic:city:create')")
    @Log(title = "城市选择表", businessType = BusinessType.INSERT)
    @PostMapping("/create")
    public R create(@RequestBody XlCity xlCity) {
        boolean success = xlCityService.create(xlCity);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    /**
     * 修改城市选择表
     */
    @PreAuthorize("@customSs.hasPermi('basic:city:edit')")
    @Log(title = "城市选择表", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/update/{cityId}")
    public R update(@PathVariable Long cityId,  @RequestBody XlCity xlCity) {
        xlCity.setCityId(cityId);
        boolean success = xlCityService.update(xlCity);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    /**
     * 删除城市选择表
     */
    @PreAuthorize("@customSs.hasPermi('basic:city:remove')")
    @Log(title = "城市选择表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cityIds}")
    public R remove(@PathVariable Long[] cityIds) {
        return R.ok(xlCityService.deleteXlCityByCityIds(cityIds));
    }
}
