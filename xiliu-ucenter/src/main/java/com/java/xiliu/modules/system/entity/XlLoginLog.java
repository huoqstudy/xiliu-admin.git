package com.java.xiliu.modules.system.entity;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import com.java.xiliu.common.entity.BaseEntity;
import com.java.xiliu.modules.system.vo.StatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 系统访问日志表对象 xl_login_log
 * 
 * @author xiliu
 * @date 2022-07-25
 */
@ExcelIgnoreUnannotated
@Data
@TableName("xl_login_log")
@ApiModel(value = "XlLoginLog对象", description = "登录日志表")
public class XlLoginLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键ID */
    @ApiModelProperty("主键ID")
    @TableId(value = "login_id", type = IdType.AUTO)
    private Long loginId;

    /** 用户账号 */
    @ExcelProperty(value = "用户账号")
    private String userCode;

    /** 登录IP地址 */
    @ExcelProperty(value = "登录IP地址")
    private String ipaddr;

    /** 登录地点 */
    @ExcelProperty(value = "登录地点")
    private String loginLocation;

    /** 浏览器类型 */
    @ExcelProperty(value = "浏览器类型")
    private String browser;

    /** 操作系统 */
    @ExcelProperty(value = "操作系统")
    private String os;

    /** 提示消息 */
    private String msg;

    /** 登录状态（0成功 1失败） */
    @ExcelProperty(value = "登录状态", converter = StatusConverter.class)
    private Integer status;

    /** 访问时间 */
    private Date loginTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /** 数据版本 */
    private Long version;




}
