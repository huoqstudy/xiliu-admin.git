package com.java.xiliu.modules.ucenter.vo;

import com.java.xiliu.common.entity.BasePageEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huoqiang
 * @description
 * @date 2021/9/26
 */
@Data
public class UserQueryVo extends BasePageEntity {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "用户名")
    private String userCode;

    @ApiModelProperty(value = "姓名")
    private String realName;
}
