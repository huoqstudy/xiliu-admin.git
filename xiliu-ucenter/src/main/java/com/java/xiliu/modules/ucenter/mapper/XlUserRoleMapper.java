package com.java.xiliu.modules.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.ucenter.entity.XlUserRole;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author xiliu
 * @since 2022-07-08
 */
public interface XlUserRoleMapper extends BaseMapper<XlUserRole> {

}
