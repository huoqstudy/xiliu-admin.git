package com.java.xiliu.modules.basicdata.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.basicdata.entity.XlVillage;

/**
 * 小区表Mapper接口
 * 
 * @author xiliu
 * @date 2022-09-16
 */
public interface XlVillageMapper extends BaseMapper<XlVillage>{
    /**
     * 查询小区表
     * @param villageId 小区表主键
     * @return 小区表
     */
    XlVillage selectXlVillageByVillageId(Long villageId);

    /**
     * 查询小区表列表
     * @param xlVillage 小区表
     * @return 小区表集合
     */
   List<XlVillage> selectXlVillageList(XlVillage xlVillage);

}
