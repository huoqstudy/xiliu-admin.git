package com.java.xiliu.modules.ucenter.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java.xiliu.common.R;
import com.java.xiliu.modules.ucenter.entity.XlRole;
import com.java.xiliu.modules.ucenter.service.XlRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author xiliu
 * @since 2022-07-01
 */
@Api(tags = "角色管理")
@RestController
@RequestMapping("/ucenter/role")
public class XlRoleController {

    @Autowired
    private XlRoleService roleService;

    @ApiOperation("添加角色")
    @PostMapping("/create")
    public R create(@RequestBody XlRole role) {
        boolean success = roleService.create(role);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    @ApiOperation("修改角色")
    @PostMapping("/update/{id}")
    public R update(@PathVariable Long id, @RequestBody XlRole role) {
        role.setRoleId(id);
        boolean success = roleService.updateById(role);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    @ApiOperation("批量删除角色")
    @PostMapping("/delete")
    public R delete(@RequestParam("ids") List<Long> ids) {
        boolean success = roleService.delete(ids);
        if (success) {
            return R.ok();
        }
        return R.error("删除失败");
    }

    @ApiOperation("获取所有角色")
    @GetMapping("/list-all")
    public R listAll() {
        List<XlRole> list = roleService.list();
        return R.ok(list);
    }

    @ApiOperation("根据角色名称分页获取角色列表")
    @GetMapping("/list-page")
    public R listPage(@RequestParam(value = "keyword", required = false) String keyword,
                      @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<XlRole> rolePage = roleService.list(keyword, pageSize, pageNum);
        return R.ok(rolePage);
    }

    @ApiOperation("修改角色状态")
    @PostMapping("/update-status/{id}")
    public R updateStatus(@PathVariable Long id, @RequestParam(value = "status") Integer status) {
        XlRole role = new XlRole();
        role.setRoleId(id);
        role.setStatus(status);
        boolean success = roleService.updateById(role);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    @ApiOperation("分配菜单权限")
    @PostMapping("/auth-role-menu")
    public R authRoleMenu(@RequestBody XlRole role) {
        boolean success = roleService.authRoleMenu(role);
        if (success) {
            return R.ok();
        }
        return R.error("分配失败");
    }
}

 