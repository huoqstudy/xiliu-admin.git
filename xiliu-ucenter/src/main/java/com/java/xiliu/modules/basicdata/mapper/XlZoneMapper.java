package com.java.xiliu.modules.basicdata.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.basicdata.entity.XlZone;

/**
 * 城区信息表Mapper接口
 * 
 * @author xiliu
 * @date 2022-09-13
 */
public interface XlZoneMapper extends BaseMapper<XlZone>{
    /**
     * 查询城区信息表
     * @param zoneId 城区信息表主键
     * @return 城区信息表
     */
    XlZone selectXlZoneByZoneId(String zoneId);

    /**
     * 查询城区信息表列表
     * @param xlZone 城区信息表
     * @return 城区信息表集合
     */
   List<XlZone> selectXlZoneList(XlZone xlZone);

}
