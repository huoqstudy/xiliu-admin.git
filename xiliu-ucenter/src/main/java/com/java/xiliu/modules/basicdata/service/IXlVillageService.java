package com.java.xiliu.modules.basicdata.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.basicdata.entity.XlVillage;

/**
 * 小区表Service接口
 * 
 * @author xiliu
 * @date 2022-09-16
 */
public interface IXlVillageService extends IService<XlVillage>{
    /**
     * 查询小区表
     * @param villageId 小区表主键
     * @return 小区表
     */
    XlVillage selectXlVillageByVillageId(Long villageId);

    /**
     * 查询小区表列表
     * @param xlVillage 小区表
     * @return 小区表集合
     */
    List<XlVillage> selectXlVillageList(XlVillage xlVillage);

    /**
     * 新增小区表
     * @param xlVillage 小区表
     * @return 结果
     */
    boolean create(XlVillage xlVillage);

    /**
     * 修改小区表
     * @param xlVillage 小区表
     * @return 结果
     */
    boolean update(XlVillage xlVillage);

    /**
     * 批量删除小区表
     * 
     * @param villageIds 需要删除的小区表主键集合
     * @return 结果
     */
    int deleteXlVillageByVillageIds(Long[] villageIds);

    /**
     * 删除小区表信息
     * 
     * @param villageId 小区表主键
     * @return 结果
     */
    int deleteXlVillageByVillageId(Long villageId);
}
