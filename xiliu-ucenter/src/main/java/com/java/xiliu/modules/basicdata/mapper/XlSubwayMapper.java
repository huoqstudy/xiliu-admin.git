package com.java.xiliu.modules.basicdata.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.basicdata.entity.XlSubway;
import org.apache.ibatis.annotations.Param;

/**
 * 地铁信息表Mapper接口
 * 
 * @author xiliu
 * @date 2022-09-16
 */
public interface XlSubwayMapper extends BaseMapper<XlSubway>{
    /**
     * 查询地铁信息表
     * @param subwayId 地铁信息表主键
     * @return 地铁信息表
     */
    XlSubway selectXlSubwayBySubwayId(String subwayId);

    /**
     * 查询地铁信息表列表
     * @param subway 地铁信息表
     * @return 地铁信息表集合
     */
   List<XlSubway> selectXlSubwayList(@Param("subway")XlSubway subway);

}
