package com.java.xiliu.modules.basicdata.controller;

import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.R;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.java.xiliu.common.annotation.Log;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.java.xiliu.modules.basicdata.entity.XlVillage;
import com.java.xiliu.modules.basicdata.service.IXlVillageService;
import com.java.xiliu.common.page.TableDataInfo;


/**
 * 小区表Controller
 * 
 * @author xiliu
 * @date 2022-09-16
 */
@Api(tags = "小区管理")
@RestController
@RequestMapping("/basic/village")
public class XlVillageController extends BaseController {
    @Autowired
    private IXlVillageService xlVillageService;

    /**
     * 查询小区表列表
     */
    @PreAuthorize("@customSs.hasPermi('basic:village:list')")
    @GetMapping("/list")
    public TableDataInfo list(XlVillage xlVillage) {
        startPage();
        List<XlVillage> list = xlVillageService.selectXlVillageList(xlVillage);
        return getDataTable(list);
    }

    /**
     * 获取小区表详细信息
     */
    @PreAuthorize("@customSs.hasPermi('basic:village:query')")
    @GetMapping(value = "/{villageId}")
    public R getInfo(@PathVariable("villageId") Long villageId) {
        return R.ok(xlVillageService.selectXlVillageByVillageId(villageId));
    }

    /**
     * 新增小区表
     */
    @PreAuthorize("@customSs.hasPermi('basic:village:create')")
    @Log(title = "小区表", businessType = BusinessType.INSERT)
    @PostMapping("/create")
    public R create(@RequestBody XlVillage xlVillage) {
        boolean success = xlVillageService.create(xlVillage);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    /**
     * 修改小区表
     */
    @PreAuthorize("@customSs.hasPermi('basic:village:edit')")
    @Log(title = "小区表", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/update/{villageId}")
    public R update(@PathVariable Long villageId,  @RequestBody XlVillage xlVillage) {
        xlVillage.setVillageId(villageId);
        boolean success = xlVillageService.update(xlVillage);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    /**
     * 删除小区表
     */
    @PreAuthorize("@customSs.hasPermi('basic:village:remove')")
    @Log(title = "小区表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{villageIds}")
    public R remove(@PathVariable Long[] villageIds) {
        return R.ok(xlVillageService.deleteXlVillageByVillageIds(villageIds));
    }
}
