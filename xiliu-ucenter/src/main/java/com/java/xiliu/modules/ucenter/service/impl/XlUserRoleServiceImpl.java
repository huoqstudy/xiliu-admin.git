package com.java.xiliu.modules.ucenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.xiliu.modules.ucenter.entity.XlUserRole;
import com.java.xiliu.modules.ucenter.mapper.XlUserRoleMapper;
import com.java.xiliu.modules.ucenter.service.XlUserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-08
 */
@Service
public class XlUserRoleServiceImpl extends ServiceImpl<XlUserRoleMapper, XlUserRole> implements XlUserRoleService {

}
