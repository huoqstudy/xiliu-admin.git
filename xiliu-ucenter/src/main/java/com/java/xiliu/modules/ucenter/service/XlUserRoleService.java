package com.java.xiliu.modules.ucenter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.ucenter.entity.XlUserRole;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-08
 */
public interface XlUserRoleService extends IService<XlUserRole> {

}
