package com.java.xiliu.modules.basicdata.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.basicdata.entity.XlSubway;

/**
 * 地铁信息表Service接口
 * 
 * @author xiliu
 * @date 2022-09-16
 */
public interface IXlSubwayService extends IService<XlSubway>{
    /**
     * 查询地铁信息表
     * @param subwayId 地铁信息表主键
     * @return 地铁信息表
     */
    XlSubway selectXlSubwayBySubwayId(String subwayId);

    /**
     * 查询地铁信息表列表
     * @param xlSubway 地铁信息表
     * @return 地铁信息表集合
     */
    List<XlSubway> selectXlSubwayList(XlSubway xlSubway);

    /**
     * 新增地铁信息表
     * @param xlSubway 地铁信息表
     * @return 结果
     */
    boolean create(XlSubway xlSubway);

    /**
     * 修改地铁信息表
     * @param xlSubway 地铁信息表
     * @return 结果
     */
    boolean update(XlSubway xlSubway);

    /**
     * 批量删除地铁信息表
     * 
     * @param subwayIds 需要删除的地铁信息表主键集合
     * @return 结果
     */
    int deleteXlSubwayBySubwayIds(String[] subwayIds);

    /**
     * 删除地铁信息表信息
     * 
     * @param subwayId 地铁信息表主键
     * @return 结果
     */
    int deleteXlSubwayBySubwayId(String subwayId);
}
