package com.java.xiliu.modules.ucenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.xiliu.modules.ucenter.entity.XlRoleMenu;
import com.java.xiliu.modules.ucenter.mapper.XlRoleMenuMapper;
import com.java.xiliu.modules.ucenter.service.XlRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-06
 */
@Service
public class XlRoleMenuServiceImpl extends ServiceImpl<XlRoleMenuMapper, XlRoleMenu> implements XlRoleMenuService {

}
