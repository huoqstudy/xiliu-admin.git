package com.java.xiliu.modules.ucenter.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.java.xiliu.modules.ucenter.entity.XlUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.ucenter.vo.UserQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author xiliu
 * @since 2021-09-03
 */
public interface XlUserMapper extends BaseMapper<XlUser> {


    /**
     * 通过用户ID查询用户
     * @param userId 用户ID
     * @return 用户对象信息
     */
    XlUser selectUserById(Long userId);

    /**
     * 根据条件分页查询用户列表
     * @param queryVo 条件信息
     * @param page 分页信息
     * @return 用户信息集合信息
     */
    List<XlUser> selectUserList(@Param("user") UserQueryVo queryVo, IPage page);
}
