package com.java.xiliu.modules.basicdata.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.Date;
import com.java.xiliu.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 城区信息表对象 xl_zone
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("xl_zone")
public class XlZone extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 主键id */
    @ApiModelProperty("主键ID")
    @TableId(value = "zone_id", type = IdType.AUTO)
    private Long zoneId;

    /** 所属城市id */
    private Long cityId;

    /** 分类父id */
    private Long parentId;

    /** 状态,1:隐藏,0:显示 */
    private String status;

    /** 排序 */
    private Long sortOrder;

    /** 分类名称 */
    private String name;

    /** 分类描述 */
    private String description;

    /** 分类层级关系路径 */
    private String path;

    /**  */
    private String x;

    /**  */
    private String y;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /** 数据版本 */
    private Long version;




}
