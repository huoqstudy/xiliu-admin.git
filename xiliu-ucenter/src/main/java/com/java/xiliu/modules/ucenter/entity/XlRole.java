package com.java.xiliu.modules.ucenter.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author xiliu
 * @since 2022-07-01
 */
@Getter
@Setter
@TableName("xl_role")
@ApiModel(value = "XlRole对象", description = "角色表")
public class XlRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色ID")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色描述")
    private String description;

    @ApiModelProperty("状态;0:禁用;1:正常")
    private Integer status;

    @ApiModelProperty("数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本人数据权限） ")
    private Integer dataScope;

    @ApiModelProperty("菜单树选择项是否关联显示（ 0：父子不互相关联显示 1：父子互相关联显示）")
    private boolean menuCheckStrictly;

    @ApiModelProperty("部门树选择项是否关联显示（0：父子不互相关联显示 1：父子互相关联显示 ）")
    private boolean deptCheckStrictly;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty("修改时间")
    private Date updatedTime;

    @ApiModelProperty("修改人的id")
    private Long updatedBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    @ApiModelProperty("创建人的id")
    private Long createdBy;

    @ApiModelProperty("逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @ApiModelProperty("数据版本")
    private Integer version;

    @ApiModelProperty("角色权限")
    private String roleKey;

    /** 菜单组 */
    @TableField(exist = false)
    private Long[] menuIds;
}
