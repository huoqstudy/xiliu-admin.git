package com.java.xiliu.modules.basicdata.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.Date;
import com.java.xiliu.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 小区表对象 xl_village
 * 
 * @author xiliu
 * @date 2022-09-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("xl_village")
public class XlVillage extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** ID编号 */
    @ApiModelProperty("主键ID")
    @TableId(value = "village_id", type = IdType.AUTO)
    private Long villageId;

    /** 小区名称 */
    private String name;

    /** 小区地址 */
    private String address;

    /** 经度 */
    private String longitude;

    /** 纬度 */
    private String latitude;

    /** 是否显示 0不显示 1显示 */
    private Long isShow;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /** 数据版本 */
    private Long version;




}
